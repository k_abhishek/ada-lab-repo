time = 0
function isbiconnected(vertex, adj[][], low[], disc[], parent[], visited[], V)
    disc[vertex]=low[vertex]=time+1
    time = time + 1
    visited[vertex]=1
    child = 0
    for i = 0 to V
        if adj[vertex][i] == 1
            if visited[i] == 0
                child = child + 1
                parent[i] = vertex
                result = isBiconnected(i, adj, low, disc, visited, V, time)
                if result == 0
                    return 0
                low[vertex] = minimum(low[vertex], low[i])
                if parent[vertex] == nil AND child > 1
                    return 0
                if parent[vertex] != nil AND low[i] >= disc[vertex]
                    return 0  
            else if parent[vertex] != i
                low[vertex] = minimum(disc[i], low[vertex])
return 1